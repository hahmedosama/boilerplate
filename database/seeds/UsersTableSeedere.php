<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'=>'ahmed',
            'email'=>'ahmed@gmail.com',
            'password'=>bcrypt('123123')
        ]);

        $user->assignRole('user');

        $admin = User::create([
            'name'=>'waqas',
            'email'=>'waqas@gmail.com',
            'password'=>bcrypt('123123')
        ]);
        $admin->assignRole('admin');

    }
}
